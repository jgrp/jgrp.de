# Technical specifications

## Techstack
* Typo3 v8.7
* PHP 7
* Composer
* Grunt
* NPM
* Sass
* Material Design Lite
* RequireJS

## Technical realisation
* Created with Typo3 v 8.7 in composer-mode with PHP 7
* The development of the Typo3 project was done local on a virtuell machine, configurated with Vagrant.
* The programmcode is saved on in files and integrated with "Include" to the backend. (No Typoscript in the database)
* The configfiles (Templates, Styling, Sprachdateien...) are saved in an extenas as Siteconfiguration. (No Templates in the 'fileadmin' - folder)
* Versioning with Git on Gitlab. Clean git workflow, which differs the current development state (master branch) from the live state (production Branch).
* Loading frontend resourced via NPM
* Compiling and minifying the CSS files from a SASS source provided from MDL via Grunt
* Full rendering in Fluid with the extension VHS - no HTML in TypoScript
* Using Typo3 v8 Form extension for contakt - formular
* Localisation (German and English)

## Security
* Outsource of the Typo3 encryption key and passwortes in external file, die outside the webroot (not versioned).


## Performance
* Minify CSS, JavaScript and HTML
* Concatenate CSS to one file
* Loading JavaScript resourcen asynchronous with RequireJS


## Responsive Features
* Using Material Design Lite as base
* Grid-Layouts with CSS Flexbox for the Media Gallery of Typo3
* Responsive images with srcset, by the extension sitegeist/sms-responsive-images

## Installed Typo3 extensions
* typo3-console
* typo3-dd-googlesitemap
* typo3-realurl
* min
* sms-responsive-images
* vhs


# jgrp base - documentation

## Install Sass

First update Ruby to > 2
https://jibai31.wordpress.com/2013/09/01/upgrading-your-vagrant-box-to-ruby-20/

then install sass with `gem install sass`
> https://www.npmjs.com/package/grunt-contrib-sass

Alternativ:
`sudo apt-get install ruby-compass`

### Error: 

*Invalid CSS after "@charset ": expected string, was "UTF-8;"*
`export LC_ALL=en_US.UTF-8` and/or `export LANG=en_US.UTF-8`


## RealURL configuration

A minimal default configuration for [RealURL](https://typo3.org/extensions/repository/view/realurl) with sensible defaults can be included in `realurl_conf.php`:

```php
require __DIR__ . '/ext/jgrp_base/Configuration/RealURL/Default.php';
```

### RealURL News configuration

To enable the RealURL configuration for the [News](https://typo3.org/extensions/repository/view/news) extension add a line like this to `realurl_conf.php`:

```php
$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl']['_DEFAULT']['fixedPostVars'][<news-pid>] = $GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl']['_DEFAULT']['fixedPostVars']['_NEWS'];
```

Here `<news-pid>` must be filled with the PID where the news plugin is located. The default setup assumes that news details are displayed on the same page and should simply extend the news list URL.


