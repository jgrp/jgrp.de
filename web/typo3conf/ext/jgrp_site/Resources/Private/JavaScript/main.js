requirejs.config({
  paths: {
    jquery: "https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min",
    "photoswip-lib" : "Lib/photoswipe.min",
    "photoswipe-ui-lib" : "Lib/photoswipe-ui-default.min",
    "photoswipe-options" : "/typo3conf/ext/jgrp_base/Resources/Public/JavaScript/PhotoSwipe_Options",
    lightbox: "/typo3conf/ext/jgrp_base/Resources/Public/JavaScript/lightbox",
    film: "/typo3conf/ext/jgrp_site/Resources/Public/JavaScript/film",
    lazyload: "/typo3conf/ext/jgrp_base/Resources/Public/JavaScript/lazyload"
  },
  shim: {
    film: ["jquery"]
  },
  // Prevent RequireJS from Caching Required Scripts
  urlArgs: "bust=v3"
});


require(["lightbox", "lazyload", "film"]);
