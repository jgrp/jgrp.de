require([
  "photoswip-lib",
  "photoswipe-ui-lib",
  "photoswipe-options",
  "jquery",
  "/typo3conf/ext/jgrp_site/Resources/Public/JavaScript/Lib/scroll.min.js",
  "https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/ScrollMagic.min.js"
], function( PhotoSwipe, PhotoSwipeUI_Default, pswpOptions, $ , Scroll, ScrollMagic) {


  if ( $('.mdl-layout').hasClass('layout-film') ) {

    var mq = window.matchMedia( "(min-height: 630px) and (min-width: 840px) and (orientation: landscape)");

    if (mq.matches) {

      var scrollPosition = 0;
      var windowHeight = $(window).height();
      var videos = [];
      var ces = $('.layout-film #content .frame-type-textmedia');
      var scroll = new Scroll($('.mdl-layout').get(0));
      var sm_controller = new ScrollMagic.Controller();


      // Remove scroll button if user scrolls with mouse. saves much pain
      window.onwheel = function (e) {
        $('.layout-film--scroll').remove();
        $(window).unbind('onwheel');
      };


      $('.layout-film .mdl-layout__content').append('<div class="layout-film--scroll down"><i class="material-icons layout-film--scroll-up">keyboard_arrow_up</i><i class="material-icons layout-film--scroll-down">keyboard_arrow_down</i></div>');

      $('.layout-film--scroll').click(function () {

        if ($(this).hasClass('down')) scrollPosition += 1;
        else if ($(this).hasClass('up')) scrollPosition = 0

        scroll.toElement(ces[scrollPosition]).then(function() {
          if (scrollPosition == ces.length-1 ) {
            $('.layout-film--scroll').addClass('up').removeClass('down');
          }
          else {
            $('.layout-film--scroll').addClass('down').removeClass('up');
          }
        })

      });


      $('.layout-film #content .frame-type-textmedia').each(function (index, value) {
        var video;
        video = $(this).find('video').get(0);
        video.loop = true;
        video.muted = true;
        video.controls = false;

        videos.push(video);

        new ScrollMagic.Scene({
          triggerElement: $(this).get(0),
          duration: $(this).height()
        })
          .addTo(sm_controller)
          .on('enter', function(e) {
            video.play();
          })
          .on('leave', function()  {
            video.pause();
        })


      });


      var pswpElement = document.querySelectorAll('.pswp')[0];
      $('.layout-film a.mdl-button').click(function (e) {
        e.preventDefault();
        var video = $(this).closest('.frame-type-textmedia').find('video').get(0);
        var openVideo = video;

        var html = [{
          html: '<div class="pswp_html-wrap"><video controls>' + openVideo.innerHTML + '</video></div>',
        }];

        var videoBox = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, html, pswpOptions);
        videoBox.init();
        $(".mdl-layout").addClass('blurred-content');
        video.pause();

        // videoBox.listen('initialZoomIn', function() {
        //   $(".mdl-layout").addClass('blurred-content');
        // });
        videoBox.listen('close', function () {
          $(".mdl-layout").removeClass('blurred-content');
          video.play();
          $('.pswp_html-wrap').remove();
        });


      });

    }
    else {
      $('section#content .mdl-button').remove();
    }
  }
});