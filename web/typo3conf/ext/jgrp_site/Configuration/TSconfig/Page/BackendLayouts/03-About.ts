mod.web_layout.BackendLayouts.About < mod.web_layout.BackendLayouts.Main
mod.web_layout.BackendLayouts.About{
  title = About
  config {
    backend_layout {
      rows {
        25 {
          columns {
            1 {
              name = Content First
              colPos = 4
              colspan = 1
            }
          }
        }
      }
    }
  }
}
