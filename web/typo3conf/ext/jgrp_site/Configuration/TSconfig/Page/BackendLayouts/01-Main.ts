mod {
  web_layout {
    BackendLayouts {
      Main >
      Main {
        title = Main

        config {
          backend_layout {
            colCount = 1
            rowCount = 40

            rows {
              10 {
                columns {
                  1 {
                    name = Hero
                    colPos = 1
                    colspan = 1
                  }
                }
              }

              20 {
                columns {
                  1 {
                    name = Content Top
                    colPos = 3
                    colspan = 1
                  }
                }
              }

              30 {
                columns {
                  1 {
                    name = Content
                    colPos = 0
                    colspan = 1
                  }
                }
              }
              40 {
                columns {
                  1 {
                    name = Content Bottom
                    colPos = 2
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}

