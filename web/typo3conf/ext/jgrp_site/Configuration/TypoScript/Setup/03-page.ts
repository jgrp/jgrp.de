page {

  meta {
    robots = noindex, nofollow
  }

  includeCSS {
    main = EXT:jgrp_site/Resources/Public/Css/main.css
  }

   includeJSLibs {
#     jquery = https://code.jquery.com/jquery-3.2.1.min.js
#     mdl = https://code.getmdl.io/1.3.0/material.min.js
   }

   includeJS {
#     main = EXT:jgrp_base/Resources/Public/JavaScript/main.js
   }

  footerData {

    # Including JavaScript
    2 = FLUIDTEMPLATE
    2 {
      file = EXT:jgrp_site/Resources/Private/Templates/Assets/JavaScript.html
      extbase.controllerExtensionName = JgrpSite
    }
  }
}


[globalVar = TSFE:page|backend_layout = pagets__Start]
  page {
    includeCSS.main >
    headerData {
      10 = TEMPLATE
      10.stdWrap.wrap = <style>|</style>
      10.template = FILE
      10.template.file = EXT:jgrp_site/Resources/Public/Css/start.css
    }
    footerData.2 >
  }
  plugin.tx_frontend._CSS_DEFAULT_STYLE >
[end]
