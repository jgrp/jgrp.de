lib {

  hero < lib.content
  hero {
   select {
     where = colPos=1
   }
  }

  contentBottom < lib.content
  contentBottom.select.where = colPos=2

  contentTop < lib.content
  contentTop.select.where = colPos=3

  contentFirst < lib.content
  contentFirst.select.where = colPos=4

  page {
    template {
      layoutRootPaths {
        20 = EXT:jgrp_site/Resources/Private/Layouts/Page
      }

      partialRootPaths {
        20 = EXT:jgrp_site/Resources/Private/Partials/Page
      }

      templateRootPaths {
        20 = EXT:jgrp_site/Resources/Private/Templates/Page
      }

      variables {
         hero =< lib.hero
         contentBottom =< lib.contentBottom
         contentTop =< lib.contentTop
         contentFirst =< lib.contentFirst
      }
    }
  }
}
