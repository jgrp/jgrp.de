config {

  linkVars = L(1)

  additionalHeaders {
    # Content-Security-Policy whitelisting sources of approved content
    # 20.header = Content-Security-Policy:default-src 'self' ; script-src 'self' 'unsafe-inline' https://cdnjs.cloudflare.com https://www.google-analytics.com/ https://www.googletagmanager.com/; connect-src https://www.googletagmanager.com/ https://www.google-analytics.com/; style-src 'self' 'unsafe-inline' https://fonts.googleapis.com; img-src 'self' https://www.google-analytics.com; font-src 'self' https://fonts.googleapis.com https://fonts.gstatic.com; frame-src 'none' ; report-uri https://jgrp.report-uri.com/r/d/csp/enforce;
  }
}

[globalVar = GP:L = 1]
config {
  language = en
  locale_all = en_US.utf8
  sys_language_uid = 1
}
[global]

